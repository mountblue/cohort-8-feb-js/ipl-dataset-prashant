function getNoOfMatchesWonPerTeamPerYear(array){
    let obj = new Object();
   
    for(let item of array){
       
        if(!obj.hasOwnProperty(item['winner']))
        {
            obj[item['winner']] = {}; 
        }
        if(!obj[item['winner']].hasOwnProperty(item['season']))
        {
            obj[item['winner']][item['season']] = 1
        }
        else
        {
            obj[item['winner']][item['season']] += 1
        }
    }
    return obj;
}
module.exports.getNoOfMatchesWonPerTeamPerYear = getNoOfMatchesWonPerTeamPerYear;