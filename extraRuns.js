function getExtraRunsPerTeamForYear(array, id){
    let obj = new Object();
    for(let item of array){
        for(let elem of id){
            if(item['match_id'] == elem){
                if(!obj.hasOwnProperty(item['bowling_team']))
                    obj[item['bowling_team']] = Number(item['extra_runs'])
                else
                    obj[item['bowling_team']] += Number(item['extra_runs'])
            }
        }
    }
    return obj;
}
module.exports.getExtraRunsPerTeamForYear = getExtraRunsPerTeamForYear;