function getNoOfMatchesPlayed(array) {
    let obj = new Object();
    for(let item of array){
        if (!obj.hasOwnProperty(item['season']))
            obj[item['season']] = 1;
        else 
            obj[item['season']] += 1;
    }
    return obj;
}

let matchid=(array)=>{
    let obj = new Object();
    for(let item of array){
        if(item['season'] == 2016){
            obj[item['id']] = item[' '];
        }
    }
    let id = Object.keys(obj)
    return id;
}

module.exports.getNoOfMatchesPlayed = getNoOfMatchesPlayed;
module.exports.matchid = matchid;