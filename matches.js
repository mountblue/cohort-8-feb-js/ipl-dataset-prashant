const matchesPlayed = require('./noOfMatchesPlayed');
const matchesWon = require('./matchesWon');
const extraruns = require('./extraRuns');
const bowlereconomy = require('./bowlerEconomy');
const csv=require('csvtojson')
csv().fromFile('matches.csv').then((jsonObj)=>{
   let matchesPlayedPerYear = matchesPlayed.getNoOfMatchesPlayed(jsonObj);
   console.log(matchesPlayedPerYear);
   let matchesWonPerTeamPerYear = matchesWon.getNoOfMatchesWonPerTeamPerYear(jsonObj);
   console.log(matchesWonPerTeamPerYear);
   let id = matchesPlayed.matchid(jsonObj);
   csv().fromFile('deliveries.csv').then((jsonObj) => {
      let m_id = [...id];
      let extra = extraruns.getExtraRunsPerTeamForYear(jsonObj, m_id)
      console.log(extra);
      let economy = bowlereconomy.getEconomicalBowlersForYear(jsonObj, m_id)
      console.log(economy);
   })
})