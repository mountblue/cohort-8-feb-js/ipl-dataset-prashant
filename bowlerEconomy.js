function getEconomicalBowlersForYear(array,id){
    let obj = new Object();
   for(let item of array){
       for(let elem of id){
           if(item['match_id'] == elem){
                if(!obj.hasOwnProperty(item['bowler']))
                    obj[item['bowler']] ={};
                if(!obj[item['bowler']].hasOwnProperty('balls')){
                    obj[item['bowler']]['balls'] = 1;
                    obj[item['bowler']]['runs'] = Number(item['noball_runs']) + Number(item['wide_runs']) + Number(item['batsman_runs'])
                }
                else{
                    obj[item['bowler']]['balls'] += 1;
                    obj[item['bowler']]['runs'] += Number(item['noball_runs']) + Number(item['wide_runs']) + Number(item['batsman_runs'])
                }
           }
       }
   }
   for(let props in obj){
       obj[props] = (obj[props]['runs'] / obj[props]['balls']) * 6;
   }
   return obj;
}
module.exports.getEconomicalBowlersForYear = getEconomicalBowlersForYear;